
import axios from 'axios'

axios.defaults.baseURL = `http://localhost:3333/`

export const getAxios = () => {
  return axios
}

export const getOrgChart = (id) =>
  getAxios()
    .get(
      `get_orgchart/${id}`
    )
    .catch(error => {
      console.error('[API](getSummaryDashboard) failed to run')
      throw error
    })